﻿using System.Collections.Generic;

namespace DataAccess
{
    public class AppSettings
    {
       
        public string Secret { get; set; }
        public string Issuer { get; set; }
    }

    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
}
