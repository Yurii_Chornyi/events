﻿namespace DataAccess.Entities
{
    public class UserEventRegistration
    {
        public int Id { get; set; }
        public Event Event { get; set; }
        public int? EventId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string City { get; set; }
        public bool OfflineParticipation { get; set; }
    }
}
