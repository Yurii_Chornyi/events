﻿using System;
using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class Organization
    {
        public Guid Id { get; set; }
        public string OrganizationUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public Guid? Administrator { get; set; }
        public ICollection<Event> Events { get; set; }
        public bool PendingRegistration { get; set; }
    }
}
