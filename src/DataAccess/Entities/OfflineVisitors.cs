﻿namespace DataAccess.Entities
{
    public class OfflineVisitors
    {
        public int Id { get; set; }
        public Event Event { get; set; }
        public UserEventRegistration User { get; set; }
    }
}
