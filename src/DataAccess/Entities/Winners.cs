﻿namespace DataAccess.Entities
{
    public class Winners
    {
        public int Id { get; set; }
        public UserEventRegistration User { get; set; }
    }
}