﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class org : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdministratorId",
                table: "Organizations");

            migrationBuilder.AddColumn<Guid>(
                name: "Administrator",
                table: "Organizations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Administrator",
                table: "Organizations");

            migrationBuilder.AddColumn<int>(
                name: "AdministratorId",
                table: "Organizations",
                nullable: false,
                defaultValue: 0);
        }
    }
}
