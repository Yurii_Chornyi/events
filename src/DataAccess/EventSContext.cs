﻿using DataAccess.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace DataAccess
{
    public class EventSContext : IdentityDbContext<User, Role, string>
    {
        private readonly ConnectionStrings _connectionStrings;

        public EventSContext(DbContextOptions<EventSContext> options, IOptions<ConnectionStrings> connectionStrings) : base(options)
        {
            _connectionStrings = connectionStrings.Value;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_connectionStrings.DefaultConnection);

            //"Host=web-events-postgresqldbserver.postgres.database.azure.com;Port=5432;Database=eventsDb;Username=postgresqldbuser@web-events-postgresqldbserver;Password=Admin123;");
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<OfflineVisitors> OfflineVisitors { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<UserEventRegistration> UserEventRegistrations { get; set; }
        public DbSet<Winners> Winners { get; set; }
    }
}
