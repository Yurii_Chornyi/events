﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
