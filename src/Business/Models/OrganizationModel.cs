﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using DataAccess.Entities;

namespace Business.Models
{
    public class OrganizationModel
    {
        public Guid Id { get; set; }
        public string OrganizationUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public Guid Administrator { get; set; }
        public ICollection<EventModel> Events { get; set; }
    }

    public class ExtendedOrganizationModel : OrganizationModel
    {
        public UserModel Admin { get; set; }
    }
}
