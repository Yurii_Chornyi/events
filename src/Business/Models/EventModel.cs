﻿namespace Business.Models
{
    public class EventModel
    {
        public int Id { get; set; }
        public OrganizationModel Organization { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
    }
}
