﻿using SendGrid.Helpers.Mail;

namespace Business.Interfaces
{
    public interface IEmailService
    {
        void Send(EmailAddress email);
    }
}
