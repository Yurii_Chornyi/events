﻿using System;
using System.Collections.Generic;
using Business.Models;

namespace Business.Interfaces
{
    public interface IEventService
    {
        int Add(EventModel eventModel);
        EventModel GetById(int eventId);
        List<EventModel> GetAllEventsByOrgId(Guid orgId);
        bool RegisterToEvent(RegistrationModel registration);
    }
}
