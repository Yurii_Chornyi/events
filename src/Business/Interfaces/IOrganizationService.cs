﻿using System;
using System.Collections.Generic;
using Business.Models;

namespace Business.Interfaces
{
    public interface IOrganizationService
    {
        Guid Add(OrganizationModel organization);
        OrganizationModel GetById(Guid orgId);
        OrganizationModel GetByName(string orgName);
        Guid GetOrgIdByUrl(string orgUrl);
        IList<OrganizationModel> GetAllOrganizations();
        IList<OrganizationModel> GetPendingOrganizations();
        OrganizationModel Register(OrganizationModel org);
    }
}
