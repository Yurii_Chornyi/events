﻿using Business.Models;
using DataAccess.Entities;

namespace Business.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string username, string password, out string token);

        UserModel Register(User user, string password);

        bool IsSysAdmin();
    }
}
