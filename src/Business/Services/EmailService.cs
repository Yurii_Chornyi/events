﻿using Business.Interfaces;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Services
{
    public class EmailService : IEmailService
    {
        private readonly SendGridClient _client;
        private readonly EmailAddress _sender;
        private readonly string _subject;

        public EmailService()
        {
            var apiKey = Environment.GetEnvironmentVariable("send_grid", EnvironmentVariableTarget.User);
            _client = new SendGridClient(apiKey);
            _sender = new EmailAddress("chornyi.yurii@gmail.com", "Yurii Chornyi");
            _subject = "Sending with SendGrid is Fun";
        }
        public async void Send(EmailAddress email)
        {
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(_sender, email, _subject, plainTextContent, htmlContent);
            var response = await _client.SendEmailAsync(msg);
        }
    }
}
