﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using DataAccess;
using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Business.Services
{
    public class UserService : IUserService// UserManager<User>, IUserService
    {
        private readonly EventSContext _dbContext;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly SignInManager<User> _signInManager;

        public UserService(EventSContext dbContext, IMapper mapper, IOptions<AppSettings> appSettings, SignInManager<User> signInManager)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _signInManager = signInManager;
        }

        public User Authenticate(string email, string password, out string token)
        {
            token = "";
            var user = _dbContext.Users.FirstOrDefault(x => x.Email == email);
            if (user == null)
            {
                throw new NullReferenceException();
            }

            var signInRes = _signInManager.PasswordSignInAsync(user, password, true, false);

            if (signInRes.Result.Succeeded)
            {
                token = GenerateToken(user.Email);
                return user;
            }

            return default;
        }

        public UserModel Register(User user, string password)
        {
            var createdUser = _signInManager.UserManager.CreateAsync(user, password);
            if (createdUser.Result.Succeeded)
            {
                var returnUser = _mapper.Map<User, UserModel>(_dbContext.Users.FirstOrDefault(x => x.Id == user.Id));
                returnUser.Token = GenerateToken(returnUser.Email);
                return returnUser;
            }
            else
            {
                throw new Exception(createdUser.Exception.Message);
            }
        }

        public bool IsSysAdmin()
        {
            return true;
        }



        #region Private methods

        private string GenerateToken(string userEmail)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userEmail),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var securityToken = new JwtSecurityToken(_appSettings.Issuer,
                _appSettings.Issuer,
                claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(securityToken);
        }

        #endregion

    }
}
