﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using DataAccess;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using SendGrid.Helpers.Mail;

namespace Business.Services
{
    public class EventService : IEventService
    {
        private readonly EventSContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;

        public EventService(EventSContext dbContext, IMapper mapper, IEmailService emailService)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _emailService = emailService;
        }

        public int Add(EventModel eventModel)
        {
            var eventId = _dbContext.Events.Add(_mapper.Map<Event>(eventModel)).Entity.Id;
            return eventId;
        }

        public EventModel GetById(int eventId)
        {
            var eventEntity = _dbContext.Events.Where(x => x.Id == eventId).AsNoTracking();
            return _mapper.Map<EventModel>(eventEntity);
        }

        public List<EventModel> GetAllEventsByOrgId(Guid orgId)
        {
            var eventEntityList = _dbContext.Events.Where(x => x.Organization.Id == orgId).AsNoTracking();
            return _mapper.Map<List<EventModel>>(eventEntityList);
        }

        public bool RegisterToEvent(RegistrationModel registration)
        {
            var eventRegistrationEntity = _mapper.Map<UserEventRegistration>(registration);
            _dbContext.UserEventRegistrations.Add(eventRegistrationEntity);
            _dbContext.SaveChanges();
            if (eventRegistrationEntity.Id != 0)
            {
                _emailService.Send(new EmailAddress(eventRegistrationEntity.Email, eventRegistrationEntity.Name));
                return true;
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}
