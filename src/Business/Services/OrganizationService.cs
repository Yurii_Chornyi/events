﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using DataAccess;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace Business.Services
{
    public class OrganizationService : IOrganizationService
    {
        private readonly DbSet<Organization> _organizationContext;
        private readonly EventSContext _dbContext;
        private readonly IMapper _mapper;

        public OrganizationService(EventSContext context, IMapper mapper)
        {
            _dbContext = context;
            _organizationContext = context.Organizations;
            _mapper = mapper;
        }

        public Guid Add(OrganizationModel organization)
        {
            var entity = _mapper.Map<Organization>(organization);
            entity.PendingRegistration = true;
            var createdEntityId = _organizationContext.Add(entity).Entity.Id;
            return createdEntityId;
        }

        public OrganizationModel GetById(Guid orgId)
        {
            var org = _organizationContext.Where(x => x.Id == orgId).AsNoTracking();
            return _mapper.Map<OrganizationModel>(org);
        }

        public OrganizationModel GetByName(string orgName)
        {
            var org = _organizationContext.Where(x => x.Name == orgName).AsNoTracking();
            return _mapper.Map<OrganizationModel>(org);
        }

        public Guid GetOrgIdByUrl(string orgUrl)
        {
            return _organizationContext.First(org => org.OrganizationUrl == orgUrl).Id;
        }

        public IList<OrganizationModel> GetAllOrganizations()
        {
            var entityList = _organizationContext.Where(x => x.PendingRegistration == false).ToList();
            return _mapper.Map<List<OrganizationModel>>(entityList);
        }

        public IList<OrganizationModel> GetPendingOrganizations()
        {
            var entityList = _organizationContext.Where(x => x.PendingRegistration).ToList();
            return _mapper.Map<List<OrganizationModel>>(entityList);
        }

        public OrganizationModel Register(OrganizationModel organization)
        {
            var org = _mapper.Map<OrganizationModel, Organization>(organization);
            org.OrganizationUrl = org.Name.Replace(" ", "_");
            _organizationContext.Add(org);
            _dbContext.SaveChanges();
            return _mapper.Map<Organization, OrganizationModel>(org);
        }
    }
}
