﻿using AutoMapper;
using Business.Models;
using DataAccess.Entities;

namespace Business
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<OrganizationModel, Organization>().ReverseMap().PreserveReferences();
            CreateMap<EventModel, Event>().ReverseMap().PreserveReferences();
            CreateMap<RegistrationModel, UserEventRegistration>().ReverseMap().PreserveReferences();
            CreateMap<UserModel, User>().ReverseMap().PreserveReferences();
        }
    }
}
