﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Web.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Run();
        }

        public static IWebHost CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                //.UseUrls("http://localhost:4000")
                .Build();
        }
    }
}