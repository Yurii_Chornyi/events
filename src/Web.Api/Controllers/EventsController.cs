﻿using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Mvc;

namespace Web.Api.Controllers
{
    [Route(apiPrefix + "events")]
    [ApiController]
    public class EventsController : BaseApiController
    {
        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpPost]
        [Route("register")]
        public IActionResult RegisterToEvent([FromBody] RegistrationModel registrationData)
        {
            return Ok(_eventService.RegisterToEvent(registrationData));
        }
    }
}