﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.Api.Controllers
{
    public class BaseApiController: ControllerBase
    {
        public const string apiPrefix = "api/";
    }
}
