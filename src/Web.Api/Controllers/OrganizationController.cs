﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Api.Controllers
{
    [Route(apiPrefix + "organization")]
    [ApiController]
    public class OrganizationController : BaseApiController
    {
        private readonly IOrganizationService _organizaionService;
        private readonly IEventService _eventService;


        public OrganizationController(IOrganizationService organizaionService, IEventService eventService)
        {
            _organizaionService = organizaionService;
            _eventService = eventService;
        }

        [HttpGet, Route("getAllOrganizations")]
        public IActionResult GetAll()
        {
            var organizations = _organizaionService.GetAllOrganizations();
            return Ok(organizations);
        }

        [HttpGet, Route("{orgUrl}/getOrganizationEvents")]
        public IActionResult GetEventsByOrgUrl(string orgUrl)
        {
            var orgId = _organizaionService.GetOrgIdByUrl(orgUrl);
            var events = _eventService.GetAllEventsByOrgId(orgId);
            return Ok(events);
        }

        [Authorize]
        [HttpPost]
        [Route("register")]
        public IActionResult Register(OrganizationModel org)
        {
            var organization = _organizaionService.Register(org);
            return Ok(organization);
        }

    }
}
