﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Business.Interfaces;
using Business.Models;
using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;

namespace Web.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route(apiPrefix + "users")]
    public class UsersController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register(UserModel userModel)
        {
            // map dto to entity
            var user = _mapper.Map<User>(userModel);

            try 
            {
                // save 
                return Ok(_userService.Register(user, userModel.Password));
                
            } 
            catch(Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody]UserModel usermodel)
        {
            // map dto to entity
            //var user = _mapper.Map<User>(userDto);

            try
            {
                // save 
                var token = "";
                var user = _mapper.Map<User, UserModel>(_userService.Authenticate(usermodel.Email, usermodel.Password,
                    out token));
                user.Token = token;

                return Ok(user);

            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("issysadmin/{userId}")]
        public IActionResult IsSysAdmin(Guid userId)
        {
            return Ok(_userService.IsSysAdmin());
        }
    }
}